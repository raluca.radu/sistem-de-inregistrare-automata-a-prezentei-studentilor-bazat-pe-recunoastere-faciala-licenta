## Descriere proiect
Proiectul își propune înregistrarea automată a prezenței studenților bazat pe recunoaștere facială, fără a fi nevoie de intervenție manuală. Prezențele vor fi stocate în fișiere Excel corespunzătoare fiecărei materii și săptămâni. De asemenea, proiectul include și crearea unei aplicații web prin care studenții, profesorii și adminii pot accesa informațiile de care au nevoie, mai exact detalii despre numărul de prezențe, respectiv utilizatori.

## Adresă GitLab
https://gitlab.upt.ro/raluca.radu/sistem-de-inregistrare-automata-a-prezentei-studentilor-bazat-pe-recunoastere-faciala-licenta

## Sistemul de înregistrare automată a prezenței studenților bazat pe recunoaștere facială

* **PRECONDIȚII:**

    **Pasul 1:** Se instalează Python.

	**Pasul 2:** Se instalează CMake și "Desktop Development with C++" din Visual Studio deoarece biblioteca Dlib are la bază C++.

	**Pasul 3:** Instalarea dependențelor

			 pip install opencv-contrib-python
			 pip install imutils
			 pip install cmake
			 pip install dlib 
			 pip install mysql-connector-python
			 pip install openpyxl
			 pip install pendulum

	**Pasul 4:** Se instalează MySQL ca și bază de date, sau se vor implementa metodele din interfața „DatabaseInterface” pentru a utiliza altă bază de date.
	
    Pentru a crea tabele se pot folosi instrucțiuni de genul pentru MySQL:
    ```
	    CREATE TABLE `face_recognition_students`.`student_schedule` (
  	     `ID` INT PRIMARY KEY NOT NULL,
  	     `Class_ID` INT,
  	     `Student_ID` INT,
   	     FOREIGN KEY (Class_ID) REFERENCES `face_recognition_students`.`classes`(Class_ID),
   	     FOREIGN KEY (Student_ID) REFERENCES `face_recognition_students`.`student`(Student_ID)
  	    );
    ```
	 Structura bazei de date poate fi văzută aici: https://uptro29158-my.sharepoint.com/:i:/g/personal/raluca_radu_student_upt_ro/EYQOCxg6rOBEuRZpwJ9NS3IBNwJ4c5skL1Ae4hypXUDJkg?e=7FSYcv


* **RULARE:**

    **Pasul 1:** În fișierul „encode_faces.py” și „recognize_faces.py” se vor completa credențialele specifice bazei de date alese. În plus, în fișierul „recognize_faces.py” se va specifica adresa RTSP a camerei de supraveghere folosite.

	Structura adresei RTSP pentru camera Hikvision utilizată: „rtsp://[USER]:[PASSWORD]@[Adresa IP a camerei]:[RTSP PORT]/Streaming/channels/101"
	
    USER, PASSWORD = credențialele de conectare la cameră (acestea pot fi omise din adresa RTSP, dacă se configurează acest lucru din portalul camerei)

    **Pasul 2:** Aplicația poate rula fișierul „recognize_faces.py” cu anumite argumente în funcție de caz. Dacă rulam cu argumentul „--perform-recognition” se va porni întreg sistemul de înregistrare automată a prezenței studenților (în terminal: „**recognize_faces.py --perform-recognition**”). Dacă se va rula cu argumentul „--insert-photos nume_student_din_baza_de_date calea_către_directorul_cu_poze”, va trebui să precizăm și cei doi parametrii specificați. Această opțiune va insera toate pozele studentului specificat în baza de date(în terminal: „**recognize_faces.py --insert-photos ”Andreea Radu” ”calea folderului ce conține pozele”** ”). Dacă se va rula cu argumentul „--generate-encodings” se va rula fișierul ce va genera codificările pentru toate pozele din baza de date(în terminal: „**recognize_faces.py --generate-encodings**”).

## Aplicația web

* **PRECONDIȚII:**

    **Pasul 1:** Se instalează Node.js

	**Pasul 2:** Din terminal, se navighează către folderul „backend” și se rulează comanda „npm install” care va instala toate dependențele

	**Pasul 3:** Din terminal, se navighează către folderul „frontend” și se rulează comanda „npm install” care va instala toate dependențele
	
	**Pasul 4:** În cazul în care apar probleme când se instalează dependențele cu „npm install”, acestea se pot instala manual în folderele:
	- frontend 
	```
		npm install axios bootstrap react-router-dom bootstrap-icons
		npm i formik react-hot-toast  	
		npm i xlsx			
		npm i form-data
		npm i react-spring		
    ```
	- backend	
    ```
		npm i express mysql2 nodemon cors cookie-parser bcrypt jsonwebtoken multer path
		npm install dotenv --save			
		npm i xlsx
		npm i nodemailer	
		npm install randomstring	
	```

	**Pasul 5:** Se instalează MySQL și se creează baza de date în același mod ca mai sus.
	
    Structura bazei de date poate fi văzută aici: https://uptro29158-my.sharepoint.com/:i:/g/personal/raluca_radu_student_upt_ro/EfXGZ2SIf8JJpDuRYd43MYgBl4xoR2h4E_BbuIKSE1CQ5Q?e=C1eoWB

* **RULARE:**

    **Pasul 1:** Pentru a porni serverul, navigăm către folderul „backend” și executăm comanda „**npm start**”

	**Pasul 2:** Pentru a porni serverul de dezvoltare, navigăm către folderul „frontend” și executăm comanda „**npm run dev**”