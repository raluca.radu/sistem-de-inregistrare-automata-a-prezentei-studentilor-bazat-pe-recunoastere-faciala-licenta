# import the necessary packages
from imutils import face_utils
import datetime
import imutils
import time
import dlib
import cv2 as cv
import pickle
from datetime import datetime
import mysql.connector
import numpy as np
from mysql.connector import Error
import openpyxl
from openpyxl import Workbook
from openpyxl.styles import PatternFill
import pendulum
import os
import argparse
import sys
from threading import Thread


class DatabaseInterface:
    def create_server_connection(self):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def read_query(self, query):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def check_if_student_has_class(self, name):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def get_current_subject(self):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def get_all_students_with_current_subject(self, subject_id):
        raise NotImplementedError("You need to implement the method in a concrete class")

    # methods used for adding multiple photos in the database of faces
    def _convert_to_binaryData(self, filename):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def _insert_one_photo(self, student_id, photo):
        raise NotImplementedError("You need to implement the method in a concrete class")

    def insert_multiple_photos_of_student(self, student_name, photos_directory_path):
        raise NotImplementedError("You need to implement the method in a concrete class")


class MySQLDatabase(DatabaseInterface):

    def __init__(self, host_name, user_name, user_password, db_name):
        self.host_name = host_name
        self.user_name = user_name
        self.user_password = user_password
        self.db_name = db_name
        self.connection = None

    def create_server_connection(self):

        try:
            self.connection = mysql.connector.connect(
                host=self.host_name,
                user=self.user_name,
                passwd=self.user_password,
                database=self.db_name
            )
            print("[INFO] The connection to the MYSQL database was successfully made!")
        except Error as err:
            print(f"[ERROR] from MySQL database connection: '{err}'")
            sys.exit(1)

    def read_query(self, query):
        if not self.connection:
            print("No database connection established")
            return

        cursor = self.connection.cursor()
        result = None

        try:
            cursor.execute(query)
            result = cursor.fetchall()
        except Error as err:
            print(f"[ERROR] while trying to execute a query: '{err}'")
            sys.exit(1)

        return result

    def check_if_student_has_class(self, name):
        query = f""" SELECT S.Name, S.Mark, M.Name, C.Day, C.Starting_hour FROM `face_recognition_students`.`student` S
                            INNER JOIN `face_recognition_students`.`student_schedule` SS ON S.Student_ID = SS.Student_ID
                            INNER JOIN `face_recognition_students`.`classes` C ON C.Class_ID = SS.Class_ID
                            INNER JOIN `face_recognition_students`.`subject` M on C.Subject_ID = M.Subject_ID
                            WHERE C.Day = DAYNAME(NOW())
                                AND TIME(NOW()) >= SUBTIME(C.Starting_hour, '00:20:00')
                                AND TIME(NOW()) <= ADDTIME(C.Starting_hour, '1:40:00')
                                AND S.Name = '{name}'"""
        return self.read_query(query)

    def get_current_subject(self):
        query_subject = """SELECT M.Subject_ID, M.Name FROM `face_recognition_students`.`classes` C
                                    INNER JOIN `face_recognition_students`.`subject` M ON C.Subject_ID = M.Subject_ID
                                    WHERE C.Day = DAYNAME(NOW())
                                    AND TIME(NOW()) >= SUBTIME(C.Starting_hour, '00:20:00')
                                    AND TIME(NOW()) <= ADDTIME(C.Starting_hour, '1:40:00')"""
        return self.read_query(query_subject)

    def get_all_students_with_current_subject(self, subject_id):
        query_students = f""" SELECT S.Name, S.Mark, M.Name FROM `face_recognition_students`.`student` S
                                    INNER JOIN `face_recognition_students`.`student_schedule` SS ON S.Student_ID = SS.Student_ID
                                    INNER JOIN `face_recognition_students`.`classes` C ON C.Class_ID = SS.Class_ID
                                    INNER JOIN `face_recognition_students`.`subject` M ON M.Subject_ID = C.Subject_ID
                                    WHERE C.Subject_ID = {subject_id} """
        return self.read_query(query_students)

    def _convert_to_binaryData(self, filename):
        try:
            with open(filename, 'rb') as file:  # open the picture in binary reading mode
                binaryData = file.read()  # read all the content of the picture and save it in the binaryData variable

            return binaryData
        except FileNotFoundError as err:
            print(f"[ERROR] File not found in dataset folder: '{err}'")
            sys.exit(1)
        except Exception as err:
            print(f"[ERROR] while converting the photos from dataset in binary data: '{err}'")
            sys.exit(1)

    def _insert_one_photo(self, student_id, photo):
        print(f"[INFO] Inserting the student photo with id {student_id} in database")
        try:
            cursor = self.connection.cursor()
            insert_query = """ INSERT INTO `face_recognition_students`.`student_faces` (Student_ID, Student_photo) 
                               VALUES (%s,%s) """

            binary_photo = self._convert_to_binaryData(photo)

            values = (student_id, binary_photo)
            result = cursor.execute(insert_query, values)
            self.connection.commit()
            print("Image and file inserted successfully as a BLOB into python_employee table", result)

        except mysql.connector.Error as error:
            print("[ERROR] Failed inserting BLOB data into MySQL table {}".format(error))
            sys.exit(1)

    def insert_multiple_photos_of_student(self, student_name, photos_directory_path):
        try:
            query = f"""SELECT Student_ID from `face_recognition_students`.`student` WHERE Name = "{student_name}" """

            student_id = self.read_query(query)[0][0]

            if student_id:
                for filename in os.listdir(photos_directory_path):
                    if filename.endswith(('.jpg', '.jpeg', 'png')):
                        self._insert_one_photo(student_id, f"{photos_directory_path}\\{filename}")
            else:
                print("[ERROR] The student's name is not correctly spelled or there is no student with this name!")
        except Exception as err:
            print(f"[ERROR] while inserting multiple photos in database: '{err}'")
            sys.exit(1)


class ExcelHandler:
    @staticmethod
    def get_maximum_rows(worksheet):
        rows = 0
        for max_row, row in enumerate(worksheet, 1):
            if not all(col.value is None for col in row):
                rows += 1

        return rows

    @staticmethod
    def load_or_create_workbook(path):
        try:
            return openpyxl.load_workbook(path)
        except FileNotFoundError:
            return Workbook()
        except Exception as err:
            print(f"[ERROR] while loading or creating a workbook: '{err}'")
            sys.exit(1)


class Attendance:
    def __init__(self, db: DatabaseInterface):
        self.db = db

    def _conditions_to_be_present(self, name):

        results = self.db.check_if_student_has_class(name)
        if results:
            return results, True

        # returns the ID and the name of the subject to be held
        subject = self.db.get_current_subject()

        if subject:
            # returns all the students that have the subject that takes place now
            students = self.db.get_all_students_with_current_subject(subject[0][0])

            return students, False
        else:
            return [], False

    def _generate_attendance_file_path(self, subject):
        try:
            path = 'prezente/' + subject + '_'

            today = pendulum.now()

            start_of_week = today.start_of('week')
            start_of_week_day = today.start_of('week').format('Do')     # Do => extract the day of week like this: 1st, 2nd, 3rd etc
            start_of_week_month = today.start_of('week').format('MMMM')
            start_of_week_year = today.start_of('week').format('YYYY')

            end_of_week = today.end_of('week')
            end_of_week_day = today.end_of('week').format('Do')
            end_of_week_month = today.end_of('week').format('MMMM')
            end_of_week_year = today.end_of('week').format('YYYY')

            if start_of_week_month != end_of_week_month and start_of_week_year == end_of_week_year:
                path += start_of_week.format('Do MMMM') + '_' + end_of_week.format('Do MMMM') + ' ' + start_of_week_year
            elif start_of_week_month != end_of_week_month and start_of_week_year != end_of_week_year:
                path += start_of_week.format('Do MMMM YYYY') + '_' + end_of_week.format('Do MMMM YYYY')
            else:
                path += start_of_week_day + '-' + end_of_week_day + ' ' + start_of_week.format('MMMM YYYY')

            path += '.xlsx'

            return path

        except Exception as e:
            print(f"[ERROR] Error while generating attendance file path: {e}")
            sys.exit(1)

    def _generate_fillers(self):
        try:
            colors = ['0099CC00', '00FFCC00']
            fillers = []

            for color in colors:
                temp = PatternFill(patternType='solid', fgColor=color)
                fillers.append(temp)

            return fillers
        except Exception as e:
            print(f"[ERROR] Error while generating attendance file path: {e}")

    def mark_attendance(self, name):
        subject = ""
        mark = ""
        # Check if the student meets all the conditions
        students = self._conditions_to_be_present(name)

        flag = 0
        if students[0]:
            for student in students[0]:
                if student[0] == name:
                    mark = student[1]
                    subject = student[2]
                    flag = 1
                    break

        if not flag:
            return

        path = self._generate_attendance_file_path(subject)

        fillers = self._generate_fillers()

        wb = ExcelHandler.load_or_create_workbook(path)
        ws = wb.active

        if ExcelHandler.get_maximum_rows(ws) == 0:
            ws.append(['Name', 'Time', 'Mark'])

        # Check if the student is already present
        nameList = []
        for row in ws.iter_rows(values_only=True):
            nameList.append(row[0])

        if name not in nameList:
            current_date_and_time = datetime.now()
            current_hour = current_date_and_time.strftime('%H:%M:%S')
            row = [name, current_hour, mark]
            ws.append(row)

            status = students[1]
            for cell in ws[ws.max_row]:
                if status:
                    cell.fill = fillers[0]
                else:
                    cell.fill = fillers[1]

        wb.save(path)


class FaceRecognition:
    def __init__(self, detector, predictor, model, data, attendance):
        self.detector = detector
        self.predictor = predictor
        self.model = model
        self.data = data
        self.attendance = attendance

    def detect_faces(self, frame):
        try:
            rects = self.detector(frame, 1)

            return rects
        except Exception as err:
            print(f"[ERROR] while detecting faces: '{err}'")
            return []

    def _get_face_descriptor(self, frame, gray_frame, rect):
        try:
            # determine the facial landmarks for each face region
            shape = self.predictor(gray_frame, rect)
            start_time = time.time()
            face_descriptor = self.model.compute_face_descriptor(frame, shape)
            end_time = time.time()
            execution_time = end_time - start_time  # Calculate execution time
            print(f"Execution time: {execution_time} seconds")

            return face_descriptor
        except Exception as err:
            print(f"[ERROR] while computing the face descriptor: '{err}'")
            return None

    def _find_possible_matches(self, face_descriptor):
        possible_matches = []
        distances = []
        try:
            # loop over the facial encodings
            for known_descriptor in self.data["encodings"]:
                # Compute Euclidean distance between the known face descriptor and computed face descriptor
                distance = np.linalg.norm(np.array(known_descriptor) - np.array(face_descriptor))
                # Set a threshold distance to determine a match
                threshold_distance = 0.6

                if distance < threshold_distance:
                    # If the distance is < threshold, we consider it a match
                    is_match = True
                else:
                    is_match = False

                distances.append(distance)
                possible_matches.append(is_match)

            return possible_matches, distances
        except Exception as err:
            print(f"[ERROR] while finding the possible matches: '{err}'")
            return [], []

    def _get_the_match_name(self, possible_matches, distances):
        try:
            name = "Unknown"

            if True in possible_matches:
                possible_matches_idx = [i for (i, match) in enumerate(possible_matches) if match]
                counts = {}

                # loop over the matched indexes and count each recognized face
                for i in possible_matches_idx:
                    possible_name = self.data["names"][i]
                    counts[possible_name] = counts.get(possible_name,
                                              0) + 1  # returns the value of the key name, if it is in the dictionary,
                                                      # if not, returns 0

                is_valid_match = 1
                flg = 0
                for nm in counts:
                    name_indices = [i for i in possible_matches_idx if self.data["names"][i] == nm]
                    distances_for_name = [distances[i] for i in name_indices]

                    if flg == 1:
                        break

                    if len(distances_for_name) <= 2:
                        name = "Unknown"
                        is_valid_match = 0  # I don't consider a match
                    else:
                        distances_for_name.sort()
                        for dst in distances_for_name:
                            if dst <= 0.52:
                                is_valid_match = 1
                                flg = 1
                                break
                            else:
                                name = "Unknown"
                                is_valid_match = 0

                if is_valid_match == 1:
                    # check if there are several keys(names) that have maximum value(number of occurrences)
                    max_value = max(counts.values())
                    keys_with_max_value = [key for key, value in counts.items() if value == max_value]

                    if len(keys_with_max_value) > 1:
                        min_distance_name = ""
                        min_distance = float('inf')
                        for i in possible_matches_idx:
                            current_name = self.data["names"][i]
                            current_distance = distances[i]

                            if current_name in keys_with_max_value and current_distance < min_distance:
                                min_distance = current_distance
                                min_distance_name = current_name

                        name = min_distance_name

                    else:
                        # the name with the maximum number of recognitions
                        name = max(counts, key=counts.get)

            return name
        except Exception as err:
            print(f"[ERROR] while trying to find the real name: '{err}'")
            return "Unknown"

    def _draw_face_box_and_name(self, frame, x, y, w, h, name):
        try:
            cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            cv.putText(frame, name, (x - 10, y - 10),
                       cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        except Exception as err:
            print(f"[ERROR] while trying to draw the box and add the name on the frame: '{err}'")

    def recognize_faces(self, frame, gray_frame, faces):
        for (i, rect) in enumerate(faces):
            face_descriptor = self._get_face_descriptor(frame, gray_frame, rect)

            if face_descriptor is None:
                continue

            possible_matches, distances = self._find_possible_matches(face_descriptor)
            name = self._get_the_match_name(possible_matches, distances)

            if name != "Unknown":
                self.attendance.mark_attendance(name)

            # convert dlib rectangle to a OpenCV bounding box
            (x, y, w, h) = face_utils.rect_to_bb(rect)
            self._draw_face_box_and_name(frame, x, y, w, h, name)


class CameraVideoStream:
    def __init__(self, name):

        self.capture = cv.VideoCapture(name)

        self.frame = None
        self.frame_count = 0

        # Start the thread to read frames from the camera
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()

    def update(self):
        while True:
            try:
                if self.capture.isOpened():
                    (self.status, self.frame) = self.capture.read()
                    self.frame_count += 1
                    print(f"[Thread] PID: {os.getpid()}, Frame Number: {self.frame_count}")
            except Exception as err:
                print(f"[ERROR] while reading the frames from camera: '{err}'")

    def show_frame(self):
        if self.status:
            frame = imutils.resize(self.frame, width=400)
            print(f"[Main Thread] PID: {os.getpid()}, Extracted Frame Number: {self.frame_count}")
            return frame
        return None

    def release_capture(self):
        self.capture.release()


def call_encoding_script():
    with open("encode_faces.py") as file:
        exec(file.read())


def main():
    # Initialize the database object
    db = MySQLDatabase("localhost", "root", "private_data", "private_data")

    # Connect to the database
    db.create_server_connection()

    parser = argparse.ArgumentParser(description="Face recognition options")
    parser.add_argument("--perform-recognition", action="store_true", help="Start face recognition logic")
    parser.add_argument("--generate-encodings", action="store_true",
                        help="Generate encodings file for the photos in database")
    parser.add_argument("--insert-photos", nargs=2, action="append",
                        metavar=("student_name", "directory_path_of_the_photos"),
                        help="Insert multiple photos of students in database")
    args = parser.parse_args()

    if not args.insert_photos and not args.generate_encodings and not args.perform_recognition:
        print("[WARNING] Run the script with -h option to see more details about how the code is run ")
        return

    if args.insert_photos:  # args.insert_photos it is a list within a list of arguments [[name, path]]
        db.insert_multiple_photos_of_student(args.insert_photos[0][0], args.insert_photos[0][1])

    if args.generate_encodings:
        call_encoding_script()

    if args.perform_recognition:
        # Initialize the attendance object
        attendance = Attendance(db)

        # Initialize the FaceRecognition object
        path_shape_predictor = "./shape_predictor_68_face_landmarks.dat"
        path_face_rec_model = "./dlib_face_recognition_resnet_model_v1.dat"

        print("[INFO] loading facial landmark predictor...")
        try:
            detector = dlib.get_frontal_face_detector()
            predictor = dlib.shape_predictor(path_shape_predictor)
            model = dlib.face_recognition_model_v1(path_face_rec_model)
        except Exception as err:
            print(f"[ERROR] during dlib initialization: '{err}'")
            sys.exit(1)

        # load the known faces and embeddings
        print("[INFO] loading encodings...")
        try:
            data = pickle.loads(open("encodings_database_testLive.pickle", "rb").read())
        except FileNotFoundError:
            print(f"[ERROR] Encoding file not found! Please add an existing file!")
            sys.exit(1)
        except Exception as err:
            print(f"[ERROR] while loading the encodings file: '{err}'")
            sys.exit(1)

        face_recognition = FaceRecognition(detector, predictor, model, data, attendance)

        # initialize the video stream from the external camera
        print("[INFO] starting video stream from external camera...")
        rtsp_url = "private_data"
        vs = CameraVideoStream(rtsp_url)
        time.sleep(2.0)

        # grab frames from the camera for processing
        while True:
            # grab the frame
            frame = vs.show_frame()

            if frame is None or frame.size == 0:
                continue

            gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

            # Face detection
            faces = face_recognition.detect_faces(gray)

            # Face recognition
            face_recognition.recognize_faces(frame, gray, faces)

            frame = imutils.resize(frame, width=600)

            # show the frame
            cv.imshow("Frame", frame)
            key = cv.waitKey(1) & 0xFF

            # if the `q` key was pressed, break from the loop
            if key == ord("q"):
                break

        # cleanup
        cv.destroyAllWindows()
        vs.release_capture()


if __name__ == "__main__":
    main()
