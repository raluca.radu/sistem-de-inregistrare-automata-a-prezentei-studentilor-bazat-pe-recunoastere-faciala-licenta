# import the necessary packages
from imutils import paths
import argparse
import pickle
import cv2 as cv
import os
import dlib
import imutils
from imutils import face_utils
import mysql.connector
import numpy as np
from mysql.connector import Error

path_to_facial_embeddings = 'encodings_database_testLive.pickle'
path_to_shape_predictor = "shape_predictor_68_face_landmarks.dat"
path_to_face_recognition_model = "dlib_face_recognition_resnet_model_v1.dat"

# initialize dlib face detector and facial landmark predictor
print("[INFO] loading facial landmark predictor...")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(path_to_shape_predictor)
model = dlib.face_recognition_model_v1(path_to_face_recognition_model)


# connecting to the MySQL server
def create_server_connection(host_name, user_name, user_password, db_name):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password,
            database=db_name
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")

    return connection


# function that will return the results from a query
def read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as err:
        print(f"Error: '{err}'")


# create the connection to the SQL database
connection = create_server_connection("localhost", "root", "private_data", "private_data")

# initialize the list of known encodings and known names
knownEncodings = []
knownNames = []

# loop over all the (name, face_image) from the database
query = """ SELECT s.Name, sf.Student_photo FROM Student s
            INNER JOIN Student_faces sf on s.Student_ID = sf.Student_ID"""
results = read_query(connection, query)

for (i, result) in enumerate(results):
    print("[INFO] processing image {}/{}".format(i + 1, len(results)))
    name = result[0]

    # Convert the binary data to a numpy array
    nparr = np.frombuffer(result[1], np.uint8)

    # Decode the numpy array to an OpenCV image
    image = cv.imdecode(nparr, cv.IMREAD_COLOR)

    # load the input image and convert it from BGR(OpenCV) to RGB(dlib)
    image = imutils.resize(image, width=400)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    boxes = detector(gray, 1)

    # loop over the face detections
    for box in boxes:
        # determine the facial landmarks for the face region
        shape = predictor(gray, box)

        # computes the vector of 128 encodings
        face_descriptors = model.compute_face_descriptor(image, shape)

        knownEncodings.append(face_descriptors)
        knownNames.append(name)

# dump the facial encodings + names to disk
print("[INFO] storing encodings...")
data = {"encodings": knownEncodings, "names": knownNames}
f = open(path_to_facial_embeddings, "wb")
f.write(pickle.dumps(data))
f.close()
