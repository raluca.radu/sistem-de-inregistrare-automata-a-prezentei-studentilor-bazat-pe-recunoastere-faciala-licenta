import os
import pickle

import numpy as np
import cv2 as cv
import dlib

path_shape_predictor = "../shape_predictor_68_face_landmarks.dat"
path_face_rec_model = "../dlib_face_recognition_resnet_model_v1.dat"

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(path_shape_predictor)
model = dlib.face_recognition_model_v1(path_face_rec_model)

data = pickle.loads(open("../encodings_database_testLive.pickle", "rb").read())


def detect_faces(frame):
    try:
        rects = detector(frame, 1)
        return rects
    except Exception as err:
        print(f"[ERROR] while detecting faces: '{err}'")
        return []

def _get_face_descriptor(frame, gray_frame, rect):
    try:
        # determine the facial landmarks for each face region
        shape = predictor(gray_frame, rect)
        face_descriptor = model.compute_face_descriptor(frame, shape)

        return face_descriptor
    except Exception as err:
        print(f"[ERROR] while computing the face descriptor: '{err}'")
        return None

def _find_possible_matches(face_descriptor):
    possible_matches = []
    distances = []
    try:
        # loop over the facial encodings
        for known_descriptor in data["encodings"]:
            # Compute Euclidean distance between the known face descriptor and computed face descriptor
            distance = np.linalg.norm(np.array(known_descriptor) - np.array(face_descriptor))
            # Set a threshold distance to determine a match
            threshold_distance = 0.6
            if distance < threshold_distance:
                # If the distance is < threshold, we consider it a match
                is_match = True
            else:
                is_match = False

            distances.append(distance)
            possible_matches.append(is_match)

        return possible_matches, distances
    except Exception as err:
        print(f"[ERROR] while finding the possible matches: '{err}'")
        return [], []

def _get_the_match_name(possible_matches, distances):
        try:
            name = "Unknown"

            if True in possible_matches:
                possible_matches_idx = [i for (i, match) in enumerate(possible_matches) if match]
                counts = {}

                # loop over the matched indexes and count each recognized face
                for i in possible_matches_idx:
                    possible_name = data["names"][i]
                    counts[possible_name] = counts.get(possible_name,
                                              0) + 1  # returneaza valoarea cheii name, daca se afla in dictionar, daca nu, returneaza 0

                is_valid_match = 1
                flg = 0
                for nm in counts:
                    name_indices = [i for i in possible_matches_idx if data["names"][i] == nm]
                    distances_for_name = [distances[i] for i in name_indices]

                    if flg == 1:
                        break

                    if len(distances_for_name) <= 2:
                        name = "Unknown"
                        is_valid_match = 0  # I don't consider a match
                    else:
                        distances_for_name.sort()
                        for dst in distances_for_name:
                            if dst <= 0.52:
                                is_valid_match = 1
                                flg = 1
                                break
                            else:
                                name = "Unknown"
                                is_valid_match = 0

                if is_valid_match == 1:
                    # check if there are several keys(names) that have maximum value(number of occurrences)
                    max_value = max(counts.values())
                    keys_with_max_value = [key for key, value in counts.items() if value == max_value]

                    if len(keys_with_max_value) > 1:
                        min_distance_name = ""
                        min_distance = float('inf')
                        for i in possible_matches_idx:
                            current_name = data["names"][i]
                            current_distance = distances[i]

                            if current_name in keys_with_max_value and current_distance < min_distance:
                                min_distance = current_distance
                                min_distance_name = current_name

                        name = min_distance_name

                    else:
                        # the name with the maximum number of recognitions
                        name = max(counts, key=counts.get)

            return name
        except Exception as err:
            print(f"[ERROR] while trying to find the real name: '{err}'")
            return "Unknown"

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img_path = os.path.join(folder, filename)
        if os.path.isfile(img_path) and img_path.lower().endswith(('.png', '.jpg', '.jpeg')):
            images.append(img_path)
    return images

def process_images(main_directory):
    y_test = []
    y_pred = []

    subdirectories = [d for d in os.listdir(main_directory) if os.path.isdir(os.path.join(main_directory, d))]

    for subdirectory in subdirectories:
        true_name = subdirectory
        folder_path = os.path.join(main_directory, subdirectory)
        images = load_images_from_folder(folder_path)

        for image_path in images:
            image = cv.imread(image_path)
            gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
            faces = detect_faces(gray)

            for face in faces:
                face_encodings = _get_face_descriptor(image, gray, face)
                possible_matches, distances = _find_possible_matches(face_encodings)
                name = _get_the_match_name(possible_matches, distances)

                y_test.append(true_name)
                y_pred.append(name)

    return y_test, y_pred

y_test, y_pred = process_images('./')

np.savetxt('./y_test.txt', y_test, fmt='%s')
np.savetxt('./y_pred.txt', y_pred, fmt='%s')

