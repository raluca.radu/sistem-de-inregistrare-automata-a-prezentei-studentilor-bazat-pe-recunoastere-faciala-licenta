import express from "express"
import cors from "cors"
import dotenv from "dotenv"
import { router2 } from "./routes/routes_secondProj.js"
import cookieParser from "cookie-parser"

const app = express()
dotenv.config()

//Middleware
app.use(express.json())
app.use(cors({
    origin: ["http://localhost:5173"],
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    credentials: true
}))
app.use(cookieParser())
app.disable('x-powered-by') //minimize exposure of the technology stack

//API routes
app.use('/api', router2)

const PORT = process.env.PORT || 3000;
app.listen(PORT, () =>{
    console.log(`Server running on PORT ${PORT}`)
})