import express from 'express'

import * as controller from '../controllers/appController.js'
import {extractUserFromToken} from '../middleware/auth.js'

const router = express.Router()

import multer from 'multer'
import path from 'path'

const upload2 = multer({dest: 'uploads/'})


/** POST Methods */
router.route('/register').post(controller.register) // register user
router.route('/login').post(controller.login) // login in app
router.route('/upload').post(upload2.single('file'),controller.upload)
router.route('/addSubjects').post(controller.addSubjects)

// GET Methods 
router.route('/protected').get(extractUserFromToken, controller.getInfo)
router.route('/userSubjects/:id').get(controller.getUserSubjects)
router.route('/userSubjectsStud/:id').get(controller.getUserSubjectsStud)
router.route('/attendancesPerStudent/:title').get(controller.getAttendancesPerStudent)
router.route('/logout').get(controller.logout)
router.route('/numberOfStudProf').get(controller.getNumberOfStudProf)
router.route('/allSubjects').get(controller.getAllSubjects)
router.route('/verifyOTP/:id').get(controller.verifyOTP)

// PUT Methods 
router.route('/updateUser').put(controller.updateUser)
router.route('/resetPassword/:id').put(controller.resetPassword)
router.route('/forgetPassword').put(controller.forgetPassword) 

// DELETE Methods
router.route('/deleteUser').delete(controller.deleteUser)

export {router as router2}