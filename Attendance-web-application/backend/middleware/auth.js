import jwt from 'jsonwebtoken'
import dotenv from 'dotenv';
dotenv.config();

export async function extractUserFromToken(req, res, next){
    const token = req.headers.authorization.split(' ')[1]

    if(!token){
        return res.status(401).json({ error : "Couldn't retrieve the token!"})
    }

    try{
        const decodedToken = await jwt.verify(token, process.env.SECRET_KEY)
        
        req.userData = { userId: decodedToken.userId, email: decodedToken.email }
        
        return next()
    } catch (error) {
        return res.status(401).json({ error : "Authentication Failed!"})
    }
    
};
