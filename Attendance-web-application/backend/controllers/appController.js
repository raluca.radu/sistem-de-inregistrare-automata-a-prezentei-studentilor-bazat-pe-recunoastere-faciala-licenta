import pool from '../utils/db.js'
import bcrypt from 'bcrypt'  //for hashing the password
import jwt from 'jsonwebtoken'
import xlsx from 'xlsx'
import dotenv from 'dotenv'
import randomstring from 'randomstring'
import nodemailer from 'nodemailer'

dotenv.config()

//POST: http://localhost:8800/api/register
export async function register(req, res) {
    try {
      
      const values = [    
        req.body.email,
        req.body.password,
        req.body.role,
        req.body.mark,
        req.body.name
      ]
      
      // Check if the email already exists
      const check_email_query = 'SELECT * FROM `aplicatie`.`users` WHERE Email = ?'
      const existEmail = await pool.promise().query(check_email_query, [values[0]])
      if (existEmail[0].length > 0) {
        return res.json({ status: false, error: 'Please use an unique email' })
      }
      
      // Check if the mark already exists
      const check_mark_query = 'SELECT * FROM `aplicatie`.`users` WHERE Mark = ?'
      const existMark = await pool.promise().query(check_mark_query, [values[3]])
      if (existMark[0].length > 0) {
        return res.json({ status: false, error: 'Please use a different mark' });
      }
     
      // Hash the password
      const hashed_password = await bcrypt.hash(values[1], 10)
  
      // Insert the new user into the database
      const insert_user_query = "INSERT INTO `aplicatie`.`users` (Email, Password, Role, Mark, Name) VALUES (?)"
      values[1] = hashed_password
      await pool.promise().query(insert_user_query, [values])

      // Insert the new user in the password recovery table
      const insert_user_for_recovery_password_query = "INSERT INTO `aplicatie`.`password_reset` (Email) VALUES (?)"
      await pool.promise().query(insert_user_for_recovery_password_query, [values[0]])
  
      // Send success response
      res.json({ status: true, msg: "User registered successfully" })
    
    } catch (error) {
      // Handle errors
      console.error('Error:', error);
      res.status(500).json({ error: 'Internal server error' })
    }
}

//POST: http://localhost:8800/api/login
export async function login(req, res) {
    try {
        const values = [    
          req.body.email,
          req.body.password,
        ]
        
        const find_user_query = "SELECT * FROM `aplicatie`.`users` WHERE Email = ?"
        const user = await pool.promise().query(find_user_query, [values[0]])

        if(user[0].length === 0){
          return res.json({ loginStatus: false, error: "Email does not exist!" })
      }

        const userPassword = user[0][0].Password
        const comparePasswords = await bcrypt.compare(values[1], userPassword)

        if(!comparePasswords){
          return res.json({ loginStatus: false, error: "Incorrect password!" })
        }

        const userID = user[0][0].ID
        const userEmail = user[0][0].Email
        
        //Create token
        const token = jwt.sign({
                            userId: userID,
                            email: userEmail
                         }, process.env.SECRET_KEY, {expiresIn: '1d'})
        
        //store the token in the browser cookie
        res.cookie('token', token)

        return res.status(200).json({loginStatus: true, role: user[0][0].Role, token: token})

      } catch (error) {
        // Handle errors
        console.error('Error:', error);
        res.status(500).json({ error: 'Internal server error' })
      }
}

//POST: http://localhost:8800/api/upload
export async function upload(req, res){
  try{
    const value = req.file

    const subject = req.file.originalname.split('_')[0]
    const date = req.file.originalname.split('_')[1].split(".")[0]
    
    const get_subject_ID_query = "SELECT Subject_ID FROM `aplicatie`.`subject` WHERE Name = ?"
    const subject_ID = await pool.promise().query(get_subject_ID_query, [subject])

    if (subject_ID[0].length === 0) {
      return res.json({ status: false, error: `Couldn\'t find the subject with the name ${subject}` })
    }

    const workbook = xlsx.readFile(value.path);
    const firstSheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[firstSheetName];
    const data = xlsx.utils.sheet_to_json(worksheet);

    for(const row of data){

      const get_user_ID_query = "SELECT ID FROM `aplicatie`.`users` WHERE Mark = ?"
      const user_ID = await pool.promise().query(get_user_ID_query, [row.Mark])
      
      if (user_ID[0].length === 0) {
        return res.json({ status: false, error: `Couldn\'t find a student with the mark ${row.Mark}` })
      }

      const query = "INSERT INTO `aplicatie`.`attendances` (User_ID, Subject_ID, Time, Week) VALUES (?, ?, ?, ?)"
      const values = [user_ID[0][0].ID, subject_ID[0][0].Subject_ID, row.Time, date]
      await pool.promise().query(query, values)
    }

    res.json({status: true, msg: "Upload succesfully!"})
  } catch (error){
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal server error' })
  }
}

//POST: http://localhost:8800/api/addSubjects
export async function addSubjects(req, res) {
  try {

    const values = [    
      req.body.name,
      req.body.mark,
      req.body.subject,
    ] 
    
    // Check if the mark already exists and extract the id and the role
    const check_mark_query = 'SELECT ID, Role FROM `aplicatie`.`users` WHERE Mark = ?'
    const result = await pool.promise().query(check_mark_query, [values[1]])
    if (result[0].length == 0) {
      return res.json({ status: false, error: 'Please use an existing mark' });
    }

    const user_id = result[0][0].ID
    const role = result[0][0].Role

    // Get subject Id
    const extract_subjectId_query = 'SELECT Subject_ID FROM `aplicatie`.`subject` WHERE Name = ?'
    const subj_id = await pool.promise().query(extract_subjectId_query, [values[2]])
    if (subj_id[0].length == 0) {
      return res.json({ status: false, error: 'Coudn\'t find the id for the chosen subject' });
    }
    const subject_id = subj_id[0][0].Subject_ID

    // Insert based on the role
    let insert_student_subject_query = ""
    if (role == 'student'){
      insert_student_subject_query = "INSERT INTO `aplicatie`.`student_subject` (User_ID, Subject_ID) VALUES (?, ?)"
    }
    else if (role == 'professor'){
      insert_student_subject_query = "INSERT INTO `aplicatie`.`teacher_subject` (User_ID, Subject_ID) VALUES (?, ?)"
    }

    const val = [user_id, subject_id]
    await pool.promise().query(insert_student_subject_query, val)
    
    res.json({ status: true, msg: "Added subject successfully" })
  
  } catch (error) {
    // Handle errors
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal server error' })
  }
}

//PUT: http://localhost:8800/api/forgetPassword
export async function forgetPassword(req, res){
  try{
    const email = req.body.email
    
    const query = "SELECT * FROM `aplicatie`.`users` WHERE Email = ?"
    const result = await pool.promise().query(query, [email])
    if (result[0].length == 0) {
      return res.json({ status: false, error: 'Please use a valid email address' });
    }
    
    const user_id = result[0][0].ID

    const otp = randomstring.generate(6)
    const otpExpier = new Date()
    otpExpier.setMinutes(otpExpier.getMinutes() + 3)

    const update_otp_query = "UPDATE `aplicatie`.`password_reset` SET token = ?, expire_at = ? WHERE email = ?"
    const [result_update] = await pool.promise().query(update_otp_query, [otp, otpExpier, email])
    
    if (result_update.changedRows === 0) {
      return res.json({status:false, error: "No changes were made" })
    }

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: 'andreea.raluca108@gmail.com',
          pass: 'vuit lane qlfl gutr',
      },
    });

    const mailOptions = {
      from: 'andreea.raluca108@gmail.com',
      to: email,
      subject: 'Resetare parolă OTP',
      html: `<p>OTP-ul dvs. (expiră în 3 minute) : <b> ${otp} </b></p>`
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return res.json({ status: false, error: `${error}` })
      } else {
          return res.json({
            status: true, msg: "OTP-ul dvs. a fost trimis pe email", user_id: user_id
          })
      }
    })

  } catch (error){
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal server error' })
  }
}

//PUT: http:localhost:8800/api/resetPassword/:id
export async function resetPassword(req, res){
  try {
    
    const ID = req.params.id
    const Password = req.body.password
    
    // Hash the new password
    const hashedPassword = await bcrypt.hash(Password, 10)
    
    // Construct SQL query to update user's password
    const update_password_query = "UPDATE `aplicatie`.`users` SET Password = ? WHERE ID = ?"
    const [result] = await pool.promise().query(update_password_query, [hashedPassword, ID])
    
    if (result.changedRows === 0) {
      return res.json.send({ error: "Error while updating the password" })
    }
    
    // Extract the role of the user
    const get_role_query = "SELECT Role FROM aplicatie.users WHERE ID = ?"
    const [role] = await pool.promise().query(get_role_query, [ID])
    
    return res.status(201).send({status: true, msg : "Password Updated", role: role[0].Role})

  } catch (error) {
      return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/verifyOTP/:id
export async function verifyOTP(req, res){
  try {
  
    const user_id = req.params.id
    const otp = req.query.otp

    //Verify if the OTP is valid
    const select_otp_time_query = `SELECT pr.token, pr.expire_at, u.ID FROM aplicatie.password_reset pr
                                           INNER JOIN aplicatie.users u WHERE pr.email = u.Email 
                                           AND u.ID = ${user_id} AND pr.token = "${otp}" AND pr.expire_at > CURTIME()`
    
    const [result] = await pool.promise().query(select_otp_time_query, [])
    
    if (result.length == 0) {
      return res.json({status:false, error: "OTP invalid or expired" })
    }

    return res.status(201).send({status: true, msg : "OTP code is correct", user_id: user_id})

  } catch (error) {
      return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/protected
export async function getInfo(req, res){
  const {userId} = req.userData
  return res.json({ id: userId })
}

//GET: http:localhost:8800/api/userSubjects/:id
export async function getUserSubjects(req, res) {
  try{
    const userID = req.params.id
    if (!userID) {
      return res.status(401).send({ error: "User Not Found!" })
    }

    const subject_query = `SELECT 
                                  s.Name AS subject_name,
                                  COUNT(ss.User_ID) AS nr_students
                            FROM aplicatie.subject s
                            JOIN aplicatie.teacher_subject ts ON s.Subject_ID = ts.Subject_ID
                            LEFT JOIN aplicatie.student_subject ss ON s.Subject_ID = ss.Subject_ID
                            WHERE ts.User_ID = ${userID}
                            GROUP BY s.Name`
    
    const result = await pool.promise().query(subject_query)

    return res.status(200).json({status: true, users: result[0]})

  }catch(error){
    console.error(error)
    return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/userSubjectsStud/:id
export async function getUserSubjectsStud(req, res) {
  try{
    const userID = req.params.id
    if (!userID) {
      return res.status(401).send({ error: "User Not Found!" })
    }

    const subject_attendances_query = `SELECT s.Name AS subject_name, COALESCE(COUNT(a.Attandance_ID), 0) AS stud_attendances FROM aplicatie.student_subject ss
         INNER JOIN aplicatie.subject s ON ss.Subject_ID = s.Subject_ID
         LEFT JOIN aplicatie.attendances a ON ss.Subject_ID = a.Subject_ID AND a.User_ID = ss.User_ID
         WHERE ss.User_ID = ${userID}
         GROUP BY s.Name`
    
    const result = await pool.promise().query(subject_attendances_query)

    return res.status(200).json({status: true, users: result[0]})

  }catch(error){
    console.error(error)
    return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/attendancesPerStudent/:title
export async function getAttendancesPerStudent(req, res) {
  try{
    const subjectName = req.params.title

    if (!subjectName) {
      return res.status(401).json({status:false, error: "Could Not Retrieve the subject name!" })
    }

    const students_attendances_query = ` SELECT u.Name AS student_name, COALESCE(COUNT(a.User_ID), 0) AS nr_attendance
			                                      FROM aplicatie.student_subject ss 
			                                      INNER JOIN aplicatie.users u on ss.User_ID = u.ID
			                                      INNER JOIN aplicatie.subject s ON ss.Subject_ID = s.Subject_ID
			                                      LEFT JOIN aplicatie.attendances a ON ss.Subject_ID = a.Subject_ID AND a.User_ID = ss.User_ID
			                                      WHERE s.Name = "${subjectName}"
			                                      GROUP BY u.Name`

    const result = await pool.promise().query(students_attendances_query)

    return res.status(200).json({status: true, student_attendances: result[0]})

  }catch (error){
    console.error(error)
    return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/numberOfStudProf
export async function getNumberOfStudProf(req, res){
  try{
    const user_total_query = `SELECT 
                                  SUM(CASE WHEN Role = "student" THEN 1 ELSE 0 END) AS student_number,
                                  SUM(CASE WHEN Role = "professor" THEN 1 ELSE 0 END) AS professor_number,
                                  SUM(CASE WHEN Role = "admin" THEN 1 ELSE 0 END) AS admin_number
                              FROM aplicatie.users`

    const result = await pool.promise().query(user_total_query)

    return res.status(200).json({status: true, user_total: result[0]})

  }catch (error){
    console.error(error)
    return res.status(500).send({ error })
  }
}

//GET: http:localhost:8800/api/allSubjects
export async function getAllSubjects(req, res){
  try{
    const extract_subjects_query= `SELECT Name FROM aplicatie.subject`

    const result = await pool.promise().query(extract_subjects_query)

    return res.status(200).json({status: true, subjects: result[0]})

  }catch (error){
    console.error(error)
    return res.status(500).send({ error })
  }
}

//PUT: http:localhost:8800/api/updateUser
export async function updateUser(req, res) {
  try {
    const body = req.body
    
    const find_user_password = "SELECT Password, Email FROM `aplicatie`.`users` WHERE Mark = ?"
    const password = await pool.promise().query(find_user_password, [body.mark])

    const email = password[0][0].Email

    if(password.length === 0){
      return res.json({status:false, error: "User not found!" })
    }
    
    if(body.password !== ""){
      const comparePasswords = await bcrypt.compare(body.password, password[0][0].Password)
      
      if(!comparePasswords){

        const hashed_password = await bcrypt.hash(body.password, 10)
        body.password = hashed_password

      } else{
        body.password = password[0][0].Password
      }
    }
    
    // Detele the fields that have "" (that field won't be updated)
    for(const key in body){
      if(!body[key]){
        delete body[key]
      }
    }

    // If the email changed, make the change in password_reset table also
    if(body.email){
      const update_password_reset_query = "UPDATE `aplicatie`.`password_reset` SET email = ? WHERE email = ?"
      const [rs] = await pool.promise().query(update_password_reset_query, [body.email, email])
    }

    // Create the SQL query to update the user record
    const update_user_query = "UPDATE `aplicatie`.`users` SET ? WHERE Mark = ?"

    // Execute the SQL query
    const [result] = await pool.promise().query(update_user_query, [body, body.mark])
    
    // Check if any rows were affected by the update
    if (result.changedRows > 0) {
      return res.json({status:true, msg: "User Updated!" })
    } else {
      return res.json({status:false, error: "No changes were made" })
    }
  } catch (error) {
    console.error(error)
    return res.status(500).send({ error })
  }
}

//GET: http://localhost:8800/api/logout
export async function logout(req, res){
   res.clearCookie('token')
   return res.status(200).send({ status: true })
}

//DELETE: http://localhost:8800/api/deleteUser
export async function deleteUser(req, res){
  try {
    
    const { name, mark, email } = req.query

    //Check if the email already exists
    const check_email_query = 'SELECT * FROM `aplicatie`.`users` WHERE Email = ?'
    const existEmail = await pool.promise().query(check_email_query, [email])
    if (existEmail[0].length === 0) {
      return res.json({ status: false, error: 'Please use an valid email' })
    }
    
    // Check if the mark already exists 
    const check_mark_query = 'SELECT ID, Role FROM `aplicatie`.`users` WHERE Mark = ?'
    const result = await pool.promise().query(check_mark_query, [mark])
    if (result[0].length == 0) {
      return res.json({ status: false, error: 'Please use an existing mark' });
    }

    const role = result[0][0].Role
    const user_id = result[0][0].ID

    let delete_query = ""
    if(role === "student"){
      delete_query = "DELETE FROM `aplicatie`.`student_subject` WHERE User_ID = ?"
    }
    else if(role === "professor"){
      delete_query = "DELETE FROM `aplicatie`.`teacher_subject` WHERE User_ID = ?"
    }
    if(role !== "admin"){
      // Delete from student_subject/teacher_subject table
      await pool.promise().query(delete_query, [user_id])
    }
    
    // Delete from attendances table
    if(role === "student"){
      const delete_query_attendances = "DELETE FROM `aplicatie`.`attendances` WHERE User_ID = ?"
      await pool.promise().query(delete_query_attendances, [user_id])
    }

    // Delete from password_reset
    const delete_password_reset_query = "DELETE FROM `aplicatie`.`password_reset` WHERE email = ?"
    await pool.promise().query(delete_password_reset_query, [email])

    // Delete from users table
    const delete_user_query = "DELETE FROM `aplicatie`.`users` WHERE ID = ?"
    await pool.promise().query(delete_user_query, [user_id])

    // Send success response
    res.json({ status: true, msg: "User deleted successfully" })
  
  } catch (error) {
    // Handle errors
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal server error' })
  }
}