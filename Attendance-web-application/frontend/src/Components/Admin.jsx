import React, { useEffect, useState } from 'react'
import Count from './ui/Count'
import axios from 'axios'

const Admin = () => {
    
    const [totalStudents, setTotalStudents] = useState(0)
    const [totalProfessor, setTotalProfessor] = useState(0)
    const [totalAdmin, setTotalAdmin] = useState(0)

    useEffect(() => {
        
        const fetchUsers = async () => {
        await axios.get(`http://localhost:8800/api/numberOfStudProf`)
            .then(response => {
            if(response.data.status){
                setTotalProfessor(parseInt(response.data.user_total[0].professor_number))
                setTotalStudents(parseInt(response.data.user_total[0].student_number))
                setTotalAdmin(parseInt(response.data.user_total[0].admin_number))
            }
            })
            .catch(err => console.log(err))
        }

        fetchUsers()
    }, [])

    
    return (
        
        <div>
            <Count
                message = " Total number of students: "
                n = {totalStudents}
            />
            <Count
                message = " Total number of professors: "
                n = {totalProfessor}
            />
            <Count
                message = " Total number of admins: "
                n = {totalAdmin}
            />
        </div>
    )
}

export default Admin