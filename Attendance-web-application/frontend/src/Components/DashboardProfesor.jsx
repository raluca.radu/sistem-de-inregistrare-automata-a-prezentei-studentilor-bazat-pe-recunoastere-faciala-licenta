import React from 'react'
import { Link, Outlet } from 'react-router-dom'
import '../styles/General.css'

const DashboardProfesor = () => {

  return (
    <div className="container-fluid">
      <div className="row flex-nowrap">
        <div className="custom-bg-color col-auto col-md-3 col-xl-2 px-sm-2 px-0 ">
          <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-black min-vh-100">
            <Link to="/dashboardProfesor"
                  className="d-flex align-items-center pb-3 mb-md-1 mt-md-3 me-md-3 me-md-auto text-black text-decoration-none"
            >
              <span className="fs-5 fw-bolder d-none d-sm-inline">
                Menu
              </span>
            </Link>
            <ul className="nav nav-pills flex-column mb-sb-auto mb-0 align-items-center align-items-sm-start"
                id="menu"
            >
              <li className="w-100">
                <Link 
                  to="subjects"
                  className="nav-link text-black px-0 align-middle"
                >
                  <i className="fs-4 bi-people ms-2"></i>
                  <span className="ms-2 d-none d-sm-inline">Subjects</span>
                </Link>
              </li>
              <li className="w-100">
                <Link 
                  to="upload"
                  className="nav-link text-black px-0 align-middle"
                >
                  <i className="fs-4 bi-columns ms-2"></i>
                  <span className="ms-2 d-none d-sm-inline">Upload</span>
                </Link>
              </li>
              <li>
                <Link 
                  to="logout"
                  className="nav-link text-black px-0 align-middle"
                >
                  <i className="fs-4 bi-power ms-2"></i>
                  <span className="ms-2 d-none d-sm-inline">Log out</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="col p-0 m-0">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default DashboardProfesor