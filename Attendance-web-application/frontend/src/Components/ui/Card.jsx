import React from 'react'
import './Card.css'
import { useNavigate } from 'react-router-dom';

export default function Card(props){
    const navigateTo = useNavigate()

    const handleOnClick = () =>{
        navigateTo(`/dashboardProfesor/details/${encodeURIComponent(props.title)}`)
    }

    return (
        <div className='card-container'>
            <h1 className='card-title'>{props.title}</h1>
            <p className='card-description'>Number of students: {props.no_students}</p>
            <button className='card-btn' onClick={handleOnClick}>See details</button>
        </div>
    )
}

