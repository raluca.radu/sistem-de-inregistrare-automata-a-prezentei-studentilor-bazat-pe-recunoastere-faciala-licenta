import React, { useEffect, useState } from 'react'
import './CardStudent.css'
import { useParams } from 'react-router-dom'


export default function CardStudent(props){
    const [isShown, setIsShown] = useState(false)

    const toggleVisibility = () =>{
        setIsShown(!isShown)
    }

    return (
        <>
            <div className='card-container'>
                <h1 className='card-title'>{props.title}</h1>
                <button className='card-btn' onClick={toggleVisibility}>See details</button>

            </div>
            {
                isShown && (
                    <div>
                        <div className="overlay"></div>
                        <div className="modal-content">
                            <h2>Current status</h2>
                            <p> Number of attendances until now: {props.nr_attendances}</p>
                            <button className='card-btn' onClick={toggleVisibility}>OK</button>
                        </div>
                    </div>
                )
            }
        </>
        
    )
}
