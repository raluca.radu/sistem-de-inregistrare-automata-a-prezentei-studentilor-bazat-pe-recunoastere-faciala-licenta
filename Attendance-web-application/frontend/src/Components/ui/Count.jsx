import React from 'react'
import {useSpring, animated} from 'react-spring'

export default function Count(props) {

    const { number } = useSpring({
        from: {number: 0},
        number: props.n,
        delay: 200, 
        config: { mass: 1, tension: 20, friction: 10},
    })

    return (
        <div className="text-center mb-3 mt-5">
            <div className="d-flex align-items-center justify-content-center">
                <h4 className="me-5 mt-4">{props.message}</h4>  
                <h1 className="display-1"> 
                    <animated.div>
                        {number.to((n) => n.toFixed(0))}
                    </animated.div>
                </h1>
                
            </div>
            
        </div>
        
  )
}
