import './Table.css'

const Table = ({ data }) => {
    return (
      <table className="table table-striped table-bordered table-hover">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Number of attendances</th>
          </tr>
          {data.map((item, index) => (
            <tr key={index}>
              <td>{item.student_name}</td>
              <td>{item.nr_attendance}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  };
  
  export default Table;