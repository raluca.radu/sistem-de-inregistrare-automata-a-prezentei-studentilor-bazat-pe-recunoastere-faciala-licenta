import React from 'react'
import './ProgressBar.css'

export default function ProgressBar(props){
    return (
        <div className="containerStyles">
            <div className="fillerStyles" style={{ width: `${props.completed}%`}}>
                <span className="labelStyles">{`${props.completed}%`}</span>
            </div>
        </div>
    );
}