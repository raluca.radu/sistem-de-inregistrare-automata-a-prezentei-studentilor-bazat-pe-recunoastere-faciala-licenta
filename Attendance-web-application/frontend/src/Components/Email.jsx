import React from 'react'
import { useNavigate } from 'react-router-dom'
import '../styles/General.css'
import { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik' 
import { emailValidate } from '../helper/validate'
import axios from 'axios'
import toast from "react-hot-toast"

const Email = () => {
    const navigate = useNavigate()

    const handleSubmit = async (values) => {
        
        await axios.put('http://localhost:8800/api/forgetPassword', values)
        .then(response => {
          if(!response.data.status){
            formik.setErrors({ email: toast.error(response.data.error) })
          }else{
            formik.resetForm()
            navigate(`/recovery/${response.data.user_id}`)
          }
        })
        .catch(err => console.log(err))
    
      }

  const formik = useFormik({
    initialValues : {
        email:'',
    },
    validate: emailValidate,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async values => {
        handleSubmit(values)
    }
  })

  return (
    <div className="container mx-auto">
        <Toaster position='top-center' reverseOrder={false}></Toaster>
        <div className='d-flex justify-content-center align-items-center vh-100'>
            <div className="glass">

                <div className="title d-flex flex-column align-items-center">
                    <h4 className="display-6 fw-bold">Recovery!</h4>
                    <span className="py-2 fs-4 text-center text-secondary w-75">
                       Enter an existing email address
                    </span>
                </div>

                <form className='pt-5' onSubmit={formik.handleSubmit}>
                    <div className="textbook d-flex flex-column align-items-center gap-5">
                        
                        <input 
                                {...formik.getFieldProps('email')} 
                                className="textbox" type='email' name='email' autoComplete='off' placeholder='Email' 
                        />
                        
                        <button className="btn" type="submit"> Send Email! </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  )
}

export default Email