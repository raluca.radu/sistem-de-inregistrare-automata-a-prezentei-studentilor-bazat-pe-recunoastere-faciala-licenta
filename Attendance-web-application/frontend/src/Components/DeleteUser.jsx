import React from 'react'
import '../styles/General.css'
import { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik' 
import { deleteUserValidate } from '../helper/validate'
import axios from 'axios'
import toast from "react-hot-toast"

const DeleteUser = () => {
    
    const handleSubmit = async (values) => {
        console.log("submit happened", values)
        const { name, mark, email } = values
        const query = `name=${encodeURIComponent(name)}&mark=${encodeURIComponent(mark)}&email=${encodeURIComponent(email)}`

        
        await axios.delete(`http://localhost:8800/api/deleteUser?${query}`)
        .then(response => {
          if(!response.data.status){
            formik.setErrors({ name: toast.error(response.data.error) })
          }else{
            toast.success(response.data.msg)
            formik.resetForm()
          }
        })
        .catch(err => console.log(err))
    
      }

    const formik = useFormik({
        initialValues : {
            name: '',
            mark: '',
            email: ''
        },
        validate: deleteUserValidate,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: values => {
          handleSubmit(values)
        }
      })

    return (
        <div className="container mx-auto">
            <Toaster position='top-center' reverseOrder={false}></Toaster>
            <div className='d-flex justify-content-center align-items-center vh-100'>
                <div className="glass">
  
                    <div className="title d-flex flex-column align-items-center">
                        <h4 className="display-6 fw-bold">Delete User!</h4>
                    </div>
  
                    <form className='py-1' onSubmit={formik.handleSubmit}>
                        <div className="textbook textbookUpload d-flex flex-column align-items-center gap-6">
                            <div className='name d-flex w-75 gap-3'>
                              <input 
                                    {...formik.getFieldProps('name')}
                                    className="textbox" type='text' name='name' placeholder='Name'
                              />
                              <input 
                                    {...formik.getFieldProps('mark')}
                                    className="textbox" type='text' name='mark' placeholder='Mark/ID'
                              />
                            </div>
                            
                            <input 
                                {...formik.getFieldProps('email')} 
                                className="textbox" type='email' name='email' placeholder='Email'
                            />
                            <button className="btn" type="submit"> Delete </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      )
}

export default DeleteUser