import React, { useEffect, useState } from 'react'
import Card from './ui/Card'
import axios from 'axios'

const Subjects = () => {

  const [subjects, setSubjects] = useState([])
  const [userID, setUserID] = useState('')

  const getTokenFromCookie = () => {
    const token = document.cookie.split("; ").find(row => row.startsWith("token=")).split("=")[1]
    return token 
  }

  useEffect(() => {
    const fetchData = async () => {
      const token = getTokenFromCookie()
      if(token){
        try{
          const response = await axios.get('http://localhost:8800/api/protected', {
              headers: {
                'Authorization': `Bearer ${token}`
              }
          })
          setUserID(response.data.id)
          
          await fetchSubjects(response.data.id)

        }catch(error){
          console.error('Error fetching data:', error);
        }
        
      } else {
        console.error('Token not found in cookies!')
      }
    }
  
    const fetchSubjects = async (id) => {
      await axios.get(`http://localhost:8800/api/userSubjects/${id}`)
        .then(response => {
          if(response.data.status){
            setSubjects(response.data.users)
            console.log(response.data.users)
          }
        })
        .catch(err => console.log(err))
    }

    fetchData()
  }, [])

  return (
    <div className = "d-flex flex-wrap gap-3">
      {
        subjects.length === 0 ? (
          <p> No data available </p>
        ) : (
        subjects.map((subject, index) => (
          <Card 
            key = {index}
            title = {subject.subject_name}
            no_students = {subject.nr_students}
          />
        ))
      )
    } 
    </div>
  )
}

export default Subjects