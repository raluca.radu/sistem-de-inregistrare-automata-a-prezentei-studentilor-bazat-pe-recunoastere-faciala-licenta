import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import Table from './ui/Table';
import '../styles/General.css'

const Details = () => {
  const { title } = useParams()
  const decodedTitle = decodeURIComponent(title)

  const [attendances, setAttendances] = useState([])
  const [query, setQuery] = useState("")
  const nr_max_prezente = 14

  useEffect(() => {
  
    const fetchAttendances = async () => {

      await axios.get(`http://localhost:8800/api/attendancesPerStudent/${decodedTitle}`)
        .then(response => {
          if(response.data.status){
            setAttendances(response.data.student_attendances)
            console.log(response.data.student_attendances)
          }
        })
        .catch(err => console.log(err))
    }

    fetchAttendances()
  }, [])

  const keys = ["student_name", "nr_attendance"]
  const search = (data) => {
    if(!query) return data

    return data.filter((item) =>
      keys.some((key) => String(item[key]).toLowerCase().includes(query))
    )
  }

  return (

    <div className='px-5 pt-5'>
      <input type="text" placeholder="Search..." 
        className="form-control-file full-width border rounded-pill p-3 mb-4" 
        onChange={(e) => setQuery(e.target.value.toLowerCase())}
      />
      {<Table data={search(attendances)} />}
    </div>
  )
}

export default Details