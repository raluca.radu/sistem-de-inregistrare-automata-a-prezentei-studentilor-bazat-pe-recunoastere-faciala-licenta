import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import '../styles/General.css'
import toast, { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik'
import { resetPasswordValidate } from '../helper/validate'
import axios from 'axios'

const Reset = () => {
  const roles = ["admin", "professor", "student"]
  const navigate = useNavigate()

    const { id } = useParams()
    const decodedID = decodeURIComponent(id)

  const handleSubmit = async (values) => {
        
    await axios.put(`http://localhost:8800/api/resetPassword/${decodedID}`, values)
    .then(response => {
        if(!response.data.status){
            toast.error(response.data.error)
        }else{
          console.log(response.data.role)
          if(response.data.role === roles[0]){
              navigate('/dashboardAdmin')
          }else if(response.data.role === roles[1]){
              navigate('/dashboardProfesor')
          }else if(response.data.role === roles[2]){
              navigate('/dashboardStudent')
          }
        }
    })
    .catch(err => console.log(err))

  }

  const formik = useFormik({
    initialValues : {
        password: '',
        confirm_password: '',
    },
    validate: resetPasswordValidate,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async values => {
        handleSubmit(values)
    }
  })

  return (
    <div className="container mx-auto">
        <Toaster position='top-center' reverseOrder={false}></Toaster>
        <div className='d-flex justify-content-center align-items-center vh-100'>
            <div className="glass">

                <div className="title d-flex flex-column align-items-center">
                    <h4 className="display-6 fw-bold">Reset Password!</h4>
                    <span className="py-2 fs-5 text-center text-secondary w-75">
                      Enter new password
                    </span>
                </div>

                <form className='pt-5' onSubmit={formik.handleSubmit}>
                    <div className="textbook d-flex flex-column align-items-center gap-6">
                        <input {...formik.getFieldProps('password')} className="textbox" type='password' name='password' placeholder='New Password'/>
                        <input {...formik.getFieldProps('confirm_password')} className="textbox" type='password' name='confirm_password' placeholder='Repeat Password'/>
                        <button className="btn" type="submit"> Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  )
}

export default Reset