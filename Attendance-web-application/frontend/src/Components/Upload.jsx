import React from 'react'
import { useNavigate } from 'react-router-dom'
import * as XLSX from 'xlsx'

import '../styles/General.css'
import '../styles/Upload.css'
import { useFormik } from 'formik'
import { fileValidate } from '../helper/validate'
import { Toaster } from 'react-hot-toast'
import toast from "react-hot-toast"
import axios from 'axios'
import FormData from 'form-data'

const Upload = () => {
  const navigate = useNavigate()
 
  const handleSubmit = async (values) => {
    const formData = new FormData()
    formData.append("file", values)
    
    await axios.post('http://localhost:8800/api/upload', formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    })
      .then(response => {
        if(!response.data.status){
          formik.setErrors({ file: toast.error(response.data.error) })
        }else{

          toast.success(response.data.msg)
          navigate('/dashboardProfesor/subjects')
        }
      })
      .catch(err => console.log(err))

  }


  const formik = useFormik({
    initialValues : {
        file: '',
    },
    validate: fileValidate,
    validateOnBlur: false,
    onSubmit: values => {
      handleSubmit(values.file)
    }
  })

  return (
    <div className="container mx-auto">
        <Toaster position='top-center' reverseOrder={false}></Toaster>
        <div className='d-flex justify-content-center align-items-center vh-100'>
            <div className="glass glassUpload">

                <div className="title d-flex flex-column align-items-center">
                    <h4 className="display-6 fw-bold">Upload files</h4>
                </div>

                <form className='py-1' onSubmit={formik.handleSubmit}>
                    <div className="textbook textbookUpload d-flex flex-column align-items-center gap-6">
                        <input 
                          type="file" className="form-control file-input" name='file'
                          onChange={ (e) => {
                            formik.setFieldValue('file', e.currentTarget.files[0])
                          }}
                          />
                        <button className="btn" type="submit"> Upload </button>
                    </div>
                </form> 

            </div>
        </div>
        
    </div>
)
}

export default Upload