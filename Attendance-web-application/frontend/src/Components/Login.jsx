import React, { useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import avatar from '../assets/images/profile.png'
import '../styles/General.css'
import { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik'
import { emailPasswordValidate } from '../helper/validate'
import axios from 'axios'
import toast from "react-hot-toast"

const Username = () => {
    const navigate = useNavigate()
    const roles = ["admin", "professor", "student"]
    axios.defaults.withCredentials = true

    const handleSubmit2 = async (values) => {

        await axios.post('http://localhost:8800/api/login', values)
            .then(result => {
                if(result.data.loginStatus){
                    if(result.data.role === roles[0]){
                        navigate('/dashboardAdmin')
                    }else if(result.data.role === roles[1]){
                        navigate('/dashboardProfesor')
                    }else if(result.data.role === roles[2]){
                        navigate('/dashboardStudent')
                    }
                    
                } else{
                    formik.setErrors({ email_password: toast.error(result.data.error) })
                }
            })
            .catch(err => console.log(err))
    }

    const formik = useFormik({
        initialValues : {
            email: '',
            password: '',
            email_password: ''
        },
        validate: emailPasswordValidate,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: values => {
            handleSubmit2(values)
        }
    })

    return (
        <div className="container mx-auto">
            <Toaster position='top-center' reverseOrder={false}></Toaster>
            <div className='d-flex justify-content-center align-items-center vh-100'>
                <div className="glass">

                    <div className="title d-flex flex-column align-items-center">
                        <h4 className="display-6 fw-bold">Welcome back!</h4>
                    </div>

                    <form className='py-1' onSubmit={formik.handleSubmit}>
                        <div className='profile d-flex justify-content-center py-3'>
                            <img src={avatar} className="profile_img" alt="avatar"/>
                        </div>

                        <div className="textbook d-flex flex-column align-items-center gap-6">
                            <input 
                                    {...formik.getFieldProps('email')} 
                                    className="textbox" type='email' name='email' autoComplete='off' placeholder='Email' 
                            />
                            <input 
                                    {...formik.getFieldProps('password')} 
                                    className="textbox" type='password' name='password' placeholder='Password'
                            />
                            <button className="btn" type="submit"> Log in </button>
                        </div>

                        <div className="text-center py-4">
                            <span 
                                className='text-secondary'>Forgot Password? 
                                <Link className='text-danger' to="/email"> Reset Now</Link>
                            </span>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    )
}

export default Username