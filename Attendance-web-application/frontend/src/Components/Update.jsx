import React from 'react'
import '../styles/General.css'
import { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik' 
import { updateValidate } from '../helper/validate'
import axios from 'axios'
import toast from "react-hot-toast"

const Update = () => {

    const possible_roles = ['admin', 'professor', 'student']

    const handleSubmit = async (values) => {

      await axios.put('http://localhost:8800/api/updateUser', values)
        .then(response => {
          if(!response.data.status){
            formik.setErrors({ email: toast.error(response.data.error) })
          }else{
            toast.success(response.data.msg)
            formik.resetForm()
          }
        })
        .catch(err => console.log(err))

    }
  

    const formik = useFormik({
      initialValues : {
          email: '',
          password: '',
          role: '',
          mark: '',
          name: ''
      },
      validate: updateValidate,
      validateOnBlur: false,
      validateOnChange: false,
      onSubmit: values => {
        handleSubmit(values)
      }
    })

    return (
      <div className="container mx-auto">
          <Toaster position='top-center' reverseOrder={false}></Toaster>
          <div className='d-flex justify-content-center align-items-center vh-100'>
              <div className="glass">

                  <div className="title d-flex flex-column align-items-center mb-5">
                      <h4 className="display-6 fw-bold">Update user!</h4>
                  </div>

                  <form className='py-1' onSubmit={formik.handleSubmit}>
                      <div className="textbook d-flex flex-column align-items-center gap-6">
                          
                          <div className='name d-flex w-75 gap-3'>
                            <input 
                                  {...formik.getFieldProps('name')}
                                  className="textbox" type='text' name='name' placeholder='Name'
                            />
                            <input 
                                  {...formik.getFieldProps('mark')}
                                  className="textbox" type='text' name='mark' placeholder='Mark/ID'
                            />
                          </div>
                          <input 
                                {...formik.getFieldProps('email')} 
                                className="textbox" type='email' name='email' placeholder='Email'
                          />
                          <input 
                                {...formik.getFieldProps('password')} 
                                className="textbox" type='password' name='password' placeholder='Password'
                          />
                          
                          <select className='form-select textbox text-secondary' {...formik.getFieldProps('role')} name='role'>
                            <option defaultValue>Choose a role</option>
                              {possible_roles.map((val,i) => {
                                return <option key={i} value = {val}>{val.charAt(0).toUpperCase() + val.slice(1)}</option>
                              })}
                          </select>
                          <button className="btn" type="submit"> Update </button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    )
}

export default Update