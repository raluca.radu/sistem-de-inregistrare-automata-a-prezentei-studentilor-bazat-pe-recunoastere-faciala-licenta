import React from 'react'
import '../styles/General.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

function Logout() {
  const navigate = useNavigate()

  const handleLogOut = async () =>{

    await axios.get('http://localhost:8800/api/logout')
      .then(response => {
        if(response.data.status){
           navigate('/')
        }
      })
      .catch(err => console.log(err))

  }

  return (
    <div className="container mx-auto">
        <div className='d-flex justify-content-center align-items-center vh-100'>
            <div className="glass glassUpload">

                <div className="title d-flex flex-column align-items-center">
                    <h4 className="display-6 fw-bold">Log out!</h4>
                    <span className="py-2 fs-4 text-center text-secondary w-75">
                        Are you sure you want to log out?
                    </span>
                </div>

                <div className='py-1' >
                    <div className="textbook textbookUpload d-flex flex-column align-items-center gap-6">
                      <button className="btn" onClick={handleLogOut}> Log out! </button>
                    </div>
                </div> 

            </div>
        </div>
        
    </div>
  )
}

export default Logout