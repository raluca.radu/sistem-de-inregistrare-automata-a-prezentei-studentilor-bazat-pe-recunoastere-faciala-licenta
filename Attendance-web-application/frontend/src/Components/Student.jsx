import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ProgressBar from './ui/ProgressBar'

function Student() {
  const [result, setResult] = useState([])
  const [userID, setUserID] = useState('')

  const getTokenFromCookie = () => {
    const token = document.cookie.split("; ").find(row => row.startsWith("token=")).split("=")[1]
    return token 
  }

  useEffect(() => {
    const fetchData = async () => {
      const token = getTokenFromCookie()
      if(token){
        try{
          const response = await axios.get('http://localhost:8800/api/protected', {
              headers: {
                'Authorization': `Bearer ${token}`
              }
          })
          setUserID(response.data.id)
          
          await fetchSubjects(response.data.id)

        }catch(error){
          console.error('Error fetching data:', error);
        }
        
      } else {
        console.error('Token not found in cookies!')
      }
    }
  
    const fetchSubjects = async (id) => {
      await axios.get(`http://localhost:8800/api/userSubjectsStud/${id}`)
        .then(response => {
          if(response.data.status){
            setResult(response.data.users)
            console.log(response.data.users)
          }
        })
        .catch(err => console.log(err))
    }

    fetchData()
  }, [])

  const getPercentage = (nr_attendances) =>{
    const idealNrOfAttendances = 14
    if(nr_attendances > idealNrOfAttendances) return 100
    else if(nr_attendances === 0) return 0
    else return (nr_attendances * 100 / idealNrOfAttendances).toFixed(2)
  }

  return (
    <div className="d-flex flex-column align-items-center">
      <h3 className="card-title">Attendances Progress</h3>
      {
        result.length === 0 ? (
          <p> No data available </p>
        ) : (
          result.map((subject, index) => (
            <div className="row align-items-center mb-3" key={index}>
              <div className="col-auto">
                <span>{subject.subject_name}</span>
              </div>
              <div className="col">
                <ProgressBar 
                  key = {index}
                  bgcolor = {"#6a1b9a"}
                  completed = {getPercentage(subject.stud_attendances)}
                />
              </div>
            </div>          
          ))
        )
    }
    </div>
  )
}

export default Student