import React, { useEffect, useState } from 'react'
import '../styles/General.css'
import { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik'
import { addSubjectValidate } from '../helper/validate'
import axios from 'axios'
import toast from "react-hot-toast"

const AddSubject = () => {

    const [subjects, setSubjects] = useState([])

    useEffect(() => {
        
        const fetchUsers = async () => {
        await axios.get(`http://localhost:8800/api/allSubjects`)
            .then(response => {
            if(response.data.status){
                const names = response.data.subjects.map(subject => subject.Name);
                setSubjects(names)
            }
            })
            .catch(err => console.log(err))
        }

        fetchUsers()
    }, [])

    const handleSubmit = async (values) => {
        
        await axios.post('http://localhost:8800/api/addSubjects', values)
        .then(response => {
          if(!response.data.status){
            formik.setErrors({ name: toast.error(response.data.error) })
          }else{
            toast.success(response.data.msg)
            formik.resetForm()
          }
        })
        .catch(err => console.log(err))
    
      }

    const formik = useFormik({
        initialValues : {
            name: '',
            mark: '',
            subject: ''
        },
        validate: addSubjectValidate,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: values => {
          handleSubmit(values)
        }
      })

    return (
        <div className="container mx-auto">
            <Toaster position='top-center' reverseOrder={false}></Toaster>
            <div className='d-flex justify-content-center align-items-center vh-100'>
                <div className="glass">
  
                    <div className="title d-flex flex-column align-items-center">
                        <h4 className="display-6 fw-bold">Add subject!</h4>
                    </div>
  
                    <form className='py-1' onSubmit={formik.handleSubmit}>
                        <div className="textbook textbookUpload d-flex flex-column align-items-center gap-6">
                            <div className='name d-flex w-75 gap-3'>
                              <input 
                                    {...formik.getFieldProps('name')}
                                    className="textbox" type='text' name='name' placeholder='Name'
                              />
                              <input 
                                    {...formik.getFieldProps('mark')}
                                    className="textbox" type='text' name='mark' placeholder='Mark/ID'
                              />
                            </div>
                            
                            <select className='form-select textbox text-secondary' {...formik.getFieldProps('subject')} name='subject'>
                              <option defaultValue>Choose a subject</option>
                                {subjects.map((val,i) => {
                                  return <option key={i} value = {val}>{val.charAt(0).toUpperCase() + val.slice(1)}</option>
                                })}
                            </select>
                            <button className="btn" type="submit"> Add subject </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      )
}

export default AddSubject