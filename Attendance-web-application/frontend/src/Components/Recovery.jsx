import React from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import '../styles/General.css'
import toast, { Toaster } from 'react-hot-toast'
import { useFormik } from 'formik' 
import { otpValidate} from '../helper/validate'
import axios from 'axios'

const Recovery = () => {

    const navigate = useNavigate()

    const { id } = useParams()
    const decodedID = decodeURIComponent(id)

    const handleSubmit = async (values) => {
        
        await axios.get(`http://localhost:8800/api/verifyOTP/${decodedID}`, {
            params: {
              otp: values.otp
            }
        })
        .then(response => {
            if(!response.data.status){
                toast.error(response.data.error)
            }else{
                formik.resetForm()
                navigate(`/reset/${response.data.user_id}`)
            }
        })
        .catch(err => console.log(err))

    }

    const formik = useFormik({
        initialValues : {
            otp: ''
        },
        validate: otpValidate,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: async values => {
            handleSubmit(values)
        }
    })

    return (
        <div className="container mx-auto">
            <Toaster position='top-center' reverseOrder={false}></Toaster>
            <div className='d-flex justify-content-center align-items-center vh-100'>
                <div className="glass">

                    <div className="title d-flex flex-column align-items-center">
                        <h4 className="display-6 fw-bold">Recovery!</h4>
                        <span className="py-2 fs-4 text-center text-secondary w-75">
                        Enter OTP to recover password
                        </span>
                    </div>

                    <form className='pt-5' onSubmit={formik.handleSubmit}>

                        <div className="textbook d-flex flex-column align-items-center gap-6">
                            <div className="input text-center">
                                <span className='py-4 text-sm text-center text-secondary'>
                                Enter 6 letters OTP sent to your email.
                                </span>
                                <input 
                                  {...formik.getFieldProps('otp')}
                                  className="textbox" type='text' name='otp' placeholder='OTP'
                                />
                            </div>
                            <button className="btn" type="submit"> Reset </button>
                        </div>

                        <div className="text-center py-4">
                            <span className='text-secondary'>Can't get OTP? <span className='text-danger text-hoverable'>
                                <Link className='text-danger' to="/email">Resend</Link></span></span>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    )
}

export default Recovery