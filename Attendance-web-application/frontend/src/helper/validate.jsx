import toast from "react-hot-toast"

// validate login page email
export async function emailPasswordValidate(values){
    const errors = verifyEmailPassword({}, values)
    return errors
}

// validate reset password 
export async function resetPasswordValidate(values){
    const errors = resetPasswordVerify({}, values)
    return errors
}

//validate register
export async function registerValidate(values){
    const errors = emailVerify({}, values)
    passwordVerify(errors, values)
    roleVerify(errors, values)
    nameAndMarkVerify(errors, values)

    return errors
}

//validate update
export async function updateValidate(values){
    const errors = markVerify({}, values)
    return errors
}

//validate file uploading
export async function fileValidate(values){
    const errors =  fileVerify({}, values)

    return errors
}

//validate adding subjects
export async function addSubjectValidate(values){
    const errors = markVerify({}, values)
    subjectVerify(errors, values)
    
    return errors
}

//validate deleting an user
export async function deleteUserValidate(values){
    const errors = markVerify({}, values)
    emailVerify(errors, values)
    
    return errors
}

//validate email
export async function emailValidate(values){
    const errors = emailVerify({}, values)
    
    return errors
}

//validate OTP
export async function otpValidate(values){
    const errors = otpVerify({}, values)
    
    return errors
}

/***************************************************************** */

// verify password and email for log in
function verifyEmailPassword(error = {}, values){
    if(!values.email && values.password){
        error.email = toast.error('Email Required!')
    } else if(values.email && !values.password){
        error.password = toast.error('Password Required!')
    } else if(!values.email && !values.password){
        error.email_password = toast.error('Email and Password Required!')
    }

    return error
}

// verify password for reset
function resetPasswordVerify(error = {}, values){
    const specialCharacters = /[` !@#$%^&*()_+\-=\[\]{};':"\\,.<>\/?~]/
    const digitsCharacters =/[0-9]/
    const uppercaseCharacters = /[A-Z]/

    if(!values.password || !values.confirm_password){
        error.password = toast.error('Password required!')
    }else if(values.password !== values.confirm_password){
        error.confirm_password = toast.error('Passwords do not match!')
    }else if(values.password.length < 7){
        error.password = toast.error('Password must have at least 7 characters!')
    }else if(!specialCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one special character!')
    }else if(!digitsCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one digit!')
    }else if(!uppercaseCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one uppercase character!')
    }

    return error
}

//verify password
function passwordVerify(error = {}, values){
    const specialCharacters = /[` !@#$%^&*()_+\-=\[\]{};':"\\,.<>\/?~]/
    const digitsCharacters =/[0-9]/
    const uppercaseCharacters = /[A-Z]/

    if(!values.password){
        error.password = toast.error('Password Required!')
    }else if(values.password.length < 7){
        error.password = toast.error('Password must have at least 7 characters!')
    }else if(!specialCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one special character!')
    }else if(!digitsCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one digit!')
    }else if(!uppercaseCharacters.test(values.password)){
        error.password = toast.error('Password must contain at least one uppercase character!')
    }

    return error
}

function roleVerify(error = {}, values){
    if(!values.role || values.role === "Choose a role"){
        error.role = toast.error('Role Required!')
    }
    return error
}

function subjectVerify(error = {}, values){
    if(!values.subject || values.subject === "Choose a subject"){
        error.role = toast.error('Subject Required!')
    }
    return error
}

function nameAndMarkVerify(error = {}, values){
    if(!values.name && !values.mark){
        error.role = toast.error('Name and Mark Required!')
    } else if(!values.name){
        error.role = toast.error('Name Required!')
    } else if(!values.mark){
        error.role = toast.error('Mark Required!')
    }
    return error
}

function markVerify(error = {}, values){
    if(!values.mark){
        error.role = toast.error('Mark is Mandatory!')
    }
    return error
}

function emailVerify(error ={}, values){
    if(!values.email){
        error.email = toast.error("Email Required!");
    }else if(values.email.includes(" ")){
        error.email = toast.error("Wrong Email!")
    }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)){
        error.email = toast.error("Invalid email address!")
    }

    return error
}

//uploaded type file verify
function fileVerify(error = {}, values){
    let fileTypes = ['application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv'] 
    
    if(!values.file){
        error.file = toast.error("File Required!")
    } else if(!fileTypes.includes(values.file.type)){
        error.file = toast.error("Please select only excel file types!")
    }

    return error
}

function otpVerify(error = {}, values){
    if(!values.otp){
        error.otp = toast.error('OTP is Mandatory!')
    }
    return error
}