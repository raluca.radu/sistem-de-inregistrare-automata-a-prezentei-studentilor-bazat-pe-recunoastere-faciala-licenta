import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import Login from './Components/Login'
import Register from './Components/Register'
import Recovery from './Components/Recovery'
import Reset from './Components/Reset'
import PageNotFound from './Components/PageNotFound'
import Student from './Components/Student'
import DashboardAdmin from './Components/DashboardAdmin'
import Update from './Components/Update'
import Logout from './Components/Logout'
import DashboardProfesor from './Components/DashboardProfesor'
import Subjects from './Components/Subjects'
import Upload from './Components/Upload'
import Details from './Components/Details'
import DashboardStudent from './Components/DashboardStudent'
import SubjectsStud from './Components/SubjectsStud'
import Admin from './Components/Admin'
import AddSubject from './Components/AddSubject'
import DeleteUser from './Components/DeleteUser'
import Email from './Components/Email'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Login/>
  }, 
  {
    path: '/email',
    element: <Email />
  },
  {
    path: '/recovery/:id',
    element: <Recovery />
  },
  {
    path: '/reset/:id',
    element: <Reset />
  },
  {
    path: '/student',
    element: <Student />
  },
  {
    path: '/dashboardAdmin',
    element: <DashboardAdmin />,
    children:[
      {
        index: true,
        element: <Admin />
      },
      {
        path: 'register',
        element: <Register />
      },
      {
        path: 'update',
        element: <Update />
      },
      {
        path: 'addSubject',
        element: <AddSubject />
      },
      {
        path: 'deleteUser',
        element: <DeleteUser />
      },
      {
        path: 'logout',
        element: <Logout />
      }
    ],
  },
  ,
  {
    path: '/dashboardProfesor',
    element: <DashboardProfesor />,
    children:[
      {
        index: true,
        element: <Subjects />
      },
      {
        path: 'subjects',
        element: <Subjects />
      },
      {
        path: 'upload',
        element: <Upload />
      },
      {
        path: 'logout',
        element: <Logout />
      },
      {
        path: 'details/:title',
        element: <Details />
      },
    ],
  },
  {
    path: '/dashboardStudent',
    element: <DashboardStudent />,
    children:[
      {
        index: true,
        element: <Student />
      },
      {
        path: 'subjectsStud',
        element: <SubjectsStud />
      },
      {
        path: 'logout',
        element: <Logout />
      },
    ],
  },
  { //invalid path into application
    path: '*',
    element: <PageNotFound />
  },
])

function App() {
  
  return (
    <main>
      <RouterProvider router={router}></RouterProvider>
    </main>
  )
}

export default App
